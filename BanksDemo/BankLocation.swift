//
//  BankLocation.swift
//  BanksDemo
//
//  Created by Artemiy Sobolev on 27.10.16.
//  Copyright © 2016 Artemiy Sobolev. All rights reserved.
//

struct BankLocation {
    
    enum LocationType: Int {
        case branch, atm, bna
    }
    
    let latitude: Double
    let longitude: Double
    let name: String
    let address: String
    let type: LocationType
    
    let region: String
    let availability: String?
    
    let info: String?
    let noCash: Bool?
    let hasCoinStation: Bool?
}

//  MARK: - Coding
extension BankLocation {
    enum Keys: String {
        case latitude = "lat", longitude = "lon", name = "n", adress = "a", type = "t",
        region = "r", availability = "av", info = "i", noCash = "ncash", hasCoinStation = "sc"
    }
    
    init(dictionary dic: [String : Any]) throws {
        latitude = try dic.value(for: Keys.latitude)
        longitude = try dic.value(for: Keys.longitude)
        name = try dic.value(for: Keys.name)
        address = try dic.value(for: Keys.adress)
        type = try dic.rawValue(for: Keys.type)
        region = try dic.value(for: Keys.region)
        availability = dic.optionalValue(for: Keys.availability)
        info = dic.optionalValue(for: Keys.info)
        noCash = dic.optionalValue(for: Keys.noCash)
        hasCoinStation = dic.optionalValue(for: Keys.hasCoinStation)
    }
}

extension BankLocation: Hashable {
    var hashValue: Int {
        
        var result = latitude.hashValue &+ longitude.hashValue &+ name.hashValue &+ address.hashValue &+ type.hashValue &+ region.hashValue
        result = result &+ (availability?.hashValue ?? 0)
        result = result &+ (info?.hashValue ?? 0)
        result = result &+ (noCash?.hashValue ?? 0)
        result = result &+ (hasCoinStation?.hashValue ?? 0)
        return result
    }
}

func ==(lhs: BankLocation, rhs: BankLocation) -> Bool {
    return lhs.latitude == rhs.latitude
        && lhs.longitude == rhs.longitude
        && lhs.name == rhs.name
        && lhs.address == rhs.address
        && lhs.type == rhs.type
        && lhs.region == rhs.region
        && lhs.availability == rhs.availability
        && lhs.info == rhs.info
        && lhs.noCash == rhs.noCash
        && lhs.hasCoinStation == rhs.hasCoinStation
}
