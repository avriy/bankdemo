//
//  Location+ComputedProperties.swift
//  BanksDemo
//
//  Created by Artemiy Sobolev on 30/10/2016.
//  Copyright © 2016 Artemiy Sobolev. All rights reserved.
//

extension Location {
    var locationType: BankLocation.LocationType {
        get {
            return BankLocation.LocationType(rawValue: Int(type))!
        } set {
            type = Int16(newValue.rawValue)
        }
    }
}

extension BankLocation.LocationType: CustomStringConvertible {
    var description: String {
        switch self {
        case .atm: return "ATM"
        case .bna: return "Bunch Note Acceptor"
        case .branch: return "Branch"
        }
    }
}

extension Int16 {
    var optionalBool: Bool? {
        switch self {
        case -1: return nil
        case 0: return false
        default: return true
        }
    }
    
    init(optionalBool: Bool?) {
        guard let bool = optionalBool else {
            self = -1
            return
        }
        self = bool ? 1 : 0
    }
}

extension Location {
    var hasCoinStation: Bool? {
        get {
            return hasCoinStationInt.optionalBool
        } set {
            hasCoinStationInt = Int16(optionalBool: newValue)
        }
    }
    
    var noCash: Bool? {
        get {
            return noCashInt.optionalBool
        } set {
            noCashInt = Int16(optionalBool: newValue)
        }
    }
}
