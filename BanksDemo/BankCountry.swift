//
//  BankCountry.swift
//  BanksDemo
//
//  Created by Artemiy Sobolev on 27.10.16.
//  Copyright © 2016 Artemiy Sobolev. All rights reserved.
//

enum BankCountry: String {
    case estonia, latvia, lithuania
}
