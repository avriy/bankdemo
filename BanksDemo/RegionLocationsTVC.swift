//
//  RegionLocationsTVC.swift
//  BanksDemo
//
//  Created by Artemiy Sobolev on 26.10.16.
//  Copyright © 2016 Artemiy Sobolev. All rights reserved.
//

import UIKit
import CoreData

class LocationCell: UITableViewCell {
    var location: Location? {
        didSet {
            textLabel?.text = location?.name
            detailTextLabel?.text = location?.address
        }
    }
}

final
class RegionLocationsTVC: UITableViewController {
    var region: Region!
    
    var fetchResultsUpdater: FetchedResultsUpdater<Location>!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = region.name
        
        let fetchedRequest: NSFetchRequest<Location> = Location.fetchRequest()
        
        fetchedRequest.predicate = NSPredicate(format: "region = %@", region)
        fetchedRequest.sortDescriptors = [NSSortDescriptor(key: "name", ascending: true)]
        
        fetchResultsUpdater = FetchedResultsUpdater(context: CoreDataStack.shared.persistentContainer.viewContext, fetchRequest: fetchedRequest)
        
        fetchResultsUpdater.performFetch(reloadHandlers: tableView.reloadHandlers(with: .automatic))
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fetchResultsUpdater.numberOfObjects(forSection: section) ?? 0
    }
    
    static let locationCellID = "LocationCellID"
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return tableView.dequeueReusableCell(withIdentifier: RegionLocationsTVC.locationCellID)!
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let cell = cell as? LocationCell else {
            fatalError("Cell has a wrong type")
        }
        
        cell.location = fetchResultsUpdater.item(indexPath: indexPath)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let ltvc = segue.destination as? LocationTVC, let cell = sender as? LocationCell {
            ltvc.location = cell.location
        }
    }
}
