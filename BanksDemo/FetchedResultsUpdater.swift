//
//  FetchedResultsUpdater.swift
//  BanksDemo
//
//  Created by Artemiy Sobolev on 29/10/2016.
//  Copyright © 2016 Artemiy Sobolev. All rights reserved.
//

import CoreData

public
class FetchedResultsUpdater<ResultType: NSFetchRequestResult>: NSObject, NSFetchedResultsControllerDelegate {
    
    private let fetchedResultsController: NSFetchedResultsController<ResultType>
    let context: NSManagedObjectContext
    
    var reloadHandlers: ReloadHandlers!
    let cache: String?
    
    public init(context: NSManagedObjectContext, fetchRequest fr: NSFetchRequest<ResultType>, sectionNameKeyPath snkp: String? = nil, cache: String? = nil) {
        self.context = context
        self.cache = cache
        assert(context.concurrencyType == .mainQueueConcurrencyType, "FetchedResultsUpdater does not support background concurrency manager")
        
        self.fetchedResultsController = NSFetchedResultsController(fetchRequest: fr, managedObjectContext: context, sectionNameKeyPath: snkp, cacheName: cache)
        super.init()
        self.fetchedResultsController.delegate = self
    }
    
    public func performFetch(reloadHandlers rh: ReloadHandlers, errorHandler eh: ErrorHandler = consoleErrorHandler) {
        reloadHandlers = rh
        do {
            try fetchedResultsController.performFetch()
            reloadHandlers.update()
        } catch let error as NSError {
            eh(error)
        }
    }
    
    public func sectionName(atIndex index: Int) -> String? {
        guard let sections = fetchedResultsController.sections else { return nil }
        guard index < sections.count else { return nil }
        return sections[index].name
    }
    
    public func updatePredicate(newPredicate: NSPredicate?) {
        if let cacheName = cache {
            NSFetchedResultsController<ResultType>.deleteCache(withName: cacheName)
        }
        
        fetchedResultsController.fetchRequest.predicate = newPredicate
        performFetch(reloadHandlers: reloadHandlers)
    }
    
    public func item(indexPath: IndexPath) -> ResultType? {
        guard let sections = fetchedResultsController.sections, indexPath.section < sections.count else { return nil }
        let section = sections[indexPath.section]
        guard let objects = section.objects as? [ResultType], indexPath.row < section.numberOfObjects else { return nil }
        return objects[indexPath.row]
    }
    
    public func indexPath(forItem item: ResultType) -> IndexPath? {
        return fetchedResultsController.indexPath(forObject: item)
    }
    
    public func items(forFilter filter: (ResultType) -> Bool) -> [ResultType]? {
        return fetchedResultsController.fetchedObjects?.filter(filter)
    }
    
    public func numberOfObjects() -> Int? {
        return fetchedResultsController.fetchedObjects?.count
    }
    
    public func numberOfObjects(forSection section: Int) -> Int? {
        return fetchedResultsController.sections?[section].numberOfObjects
    }
    
    public func numberOfSections() -> Int? {
        return fetchedResultsController.sections?.count
    }
    
    public func removeItem(indexPath: IndexPath) {
        guard let item = item(indexPath: indexPath) as? NSManagedObject else {
            return
        }
        context.delete(item)
    }
    
    public func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .insert:
            reloadHandlers.insertItemsAtIndexPaths([newIndexPath!])
        case .delete:
            reloadHandlers.deleteItemsAtIndexPaths([indexPath!])
        case .update:
            reloadHandlers.updateItemsAtIndexPaths([indexPath!])
        case .move:
            reloadHandlers.moveItems(indexPath!, newIndexPath!)
        }
    }
    
    public func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        switch type {
        case .insert:
            reloadHandlers.insertSection(sectionIndex)
        case .update:
            reloadHandlers.updateSection(sectionIndex)
        case .delete:
            reloadHandlers.deleteSection(sectionIndex)
        case .move:
            assertionFailure("not handled case")
        }
    }
    
    public func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        reloadHandlers.willStartUpdates()
    }
    
    public func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        reloadHandlers.didEndUpdates()
    }
}

