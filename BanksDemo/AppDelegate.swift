//
//  AppDelegate.swift
//  BanksDemo
//
//  Created by Artemiy Sobolev on 26.10.16.
//  Copyright © 2016 Artemiy Sobolev. All rights reserved.
//

import UIKit
import CoreData

class CoreDataStack {
    static let shared = CoreDataStack()
    
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "BanksDemo")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    func saveContext () {
        let context = persistentContainer.viewContext
        guard context.hasChanges else { return }
        do {
            try context.save()
        } catch {
            let nserror = error as NSError
            fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
        }
    }
    
    func performTaskWithBackgroundContext(context: @escaping (NSManagedObjectContext) -> Void) {
        let newBackgroundContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        newBackgroundContext.parent = persistentContainer.viewContext
        newBackgroundContext.perform {
            context(newBackgroundContext)
        }
    }
    
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    func applicationWillTerminate(_ application: UIApplication) {
        CoreDataStack.shared.saveContext()
    }
}

