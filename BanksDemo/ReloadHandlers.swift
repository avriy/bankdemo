//
//  ReloadHandlers.swift
//  BanksDemo
//
//  Created by Artemiy Sobolev on 29/10/2016.
//  Copyright © 2016 Artemiy Sobolev. All rights reserved.
//

import Foundation

public
struct ReloadHandlers {
    let insertItemsAtIndexPaths: ([IndexPath]) -> Void
    let deleteItemsAtIndexPaths: ([IndexPath]) -> Void
    let updateItemsAtIndexPaths: ([IndexPath]) -> Void
    let moveItems: (_ fromIndexPath: IndexPath, _ toIndexPath: IndexPath) -> Void
    
    let insertSection: (Int) -> Void
    let deleteSection: (Int) -> Void
    let updateSection: (Int) -> Void
    
    let update: (Void) -> Void
    let willStartUpdates: (Void) -> Void
    let didEndUpdates: (Void) -> Void
}

func +(lhs: ReloadHandlers, rhs: ReloadHandlers) -> ReloadHandlers {
    return ReloadHandlers(firstReloadHandler: lhs, secondReloadHandler: rhs)
}

private
func handlerComposition<T>(f1: @escaping (T) -> Void, f2: @escaping (T) -> Void) -> ((T) -> Void) {
    return {
        f1($0)
        f2($0)
    }
}

public
extension ReloadHandlers {
    
    init(firstReloadHandler frh: ReloadHandlers, secondReloadHandler srh: ReloadHandlers) {
        insertItemsAtIndexPaths = handlerComposition(f1: frh.insertItemsAtIndexPaths, f2: srh.insertItemsAtIndexPaths)
        deleteItemsAtIndexPaths = handlerComposition(f1: frh.deleteItemsAtIndexPaths, f2: srh.deleteItemsAtIndexPaths)
        updateItemsAtIndexPaths = handlerComposition(f1: frh.updateItemsAtIndexPaths, f2: srh.updateItemsAtIndexPaths)
        moveItems = handlerComposition(f1: frh.moveItems, f2: srh.moveItems)
        
        insertSection = handlerComposition(f1: frh.insertSection, f2: srh.insertSection)
        deleteSection = handlerComposition(f1: frh.deleteSection, f2: srh.deleteSection)
        updateSection = handlerComposition(f1: frh.updateSection, f2: srh.updateSection)
        
        update = handlerComposition(f1: frh.update, f2: srh.update)
        willStartUpdates = handlerComposition(f1: frh.willStartUpdates, f2: srh.willStartUpdates)
        didEndUpdates = handlerComposition(f1: frh.didEndUpdates, f2: srh.didEndUpdates)
    }
    
    
    init(collectionCountChangingHandler h: @escaping (Void) -> Void) {
        let indexPathHandler: ([IndexPath]) -> Void = { _ in
            h()
        }
        
        self.init(insertItemsAtIndexPaths: indexPathHandler, deleteItemsAtIndexPaths: indexPathHandler, updateItemsAtIndexPaths: indexPathHandler, moveItems: { _ in },
                  insertSection: { _ in },
                  deleteSection: { _ in },
                  updateSection: { _ in },
                  update: h,
                  willStartUpdates: { _ in },
                  didEndUpdates: { _ in })
    }
}

