//
//  LocationLoader.swift
//  BanksDemo
//
//  Created by Artemiy Sobolev on 29/10/2016.
//  Copyright © 2016 Artemiy Sobolev. All rights reserved.
//

import Foundation

typealias LocationLoaderHandler = ((country: BankCountry, locations: [BankLocation])) -> Void
typealias ErrorHandler = (NSError) -> Void

let consoleErrorHandler: ErrorHandler = {
    print("Error happened: \($0)")
}

final
class LocationsLoader {
    private let countries: [BankCountry]
    private let session: URLSession
    private let cookieStorage: HTTPCookieStorage
    
    
    init(session: URLSession = .shared, cookieStorage: HTTPCookieStorage = .shared, countries: [BankCountry]) {
        self.countries = countries; self.session = session; self.cookieStorage = cookieStorage
    }
    
    func load(perCountryHandler pch: @escaping LocationLoaderHandler, completionHandler: @escaping ([NSError]) -> Void) {
        let cookies = countries.map { $0.cookie() }
        
        cookies.forEach(cookieStorage.setCookie)
        let group = DispatchGroup()
        
        
        var errors = [NSError]()

        let perCountryErrorHandler: (NSError) -> Void = {
            errors.append($0)
            group.leave()
        }
        
        
        for country in countries {
            group.enter()
            let dataTask = session.dataTask(for: country, errorHandler: perCountryErrorHandler) { (contry, data) in
                do {
                    let objects = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [[String : Any]]
                    let locations: [BankLocation] = objects.flatMap { try? BankLocation(dictionary: $0) }
                    pch((country: country, locations: locations))
                } catch let error as NSError {
                    perCountryErrorHandler(error)
                }
                group.leave()
            }
            dataTask.resume()
        }
        
        group.notify(queue: .main) {
            completionHandler(errors)
        }
    }
}

private
extension BankCountry {
    private static let swedEmbeeded = "Swedbank-Embedded"
    private static let iphoneApp = "iphone-app"
    
    var webPath: String {
        switch self {
        case .estonia: return BankCountry.estoniaPath
        case .latvia: return BankCountry.latviaPath
        case .lithuania: return BankCountry.lithuaniaPath
        }
    }
    
    private static let estoniaPath = "https://www.swedbank.ee/finder.json"
    private static let latviaPath = "https://ib.swedbank.lv/finder.json"
    private static let lithuaniaPath = "https://ib.swedbank.lt/finder.json"
    
    var url: URL {
        return URL(string: webPath)!
    }
    
    func cookie() -> HTTPCookie {
        return HTTPCookie(properties: [ .name: BankCountry.swedEmbeeded, .value: BankCountry.iphoneApp, .path: "/", .originURL: url])!
    }
}

private
extension URLSession {
    func dataTask(for country: BankCountry, errorHandler: @escaping (NSError) -> Void, successHandler: @escaping (BankCountry, Data) -> Void) -> URLSessionDataTask {
        return dataTask(with: country.url) { (data, response, error) in
            if let data = data {
                return successHandler(country, data)
            } else if let error = error {
                return errorHandler(error as NSError)
            }
        }
    }
}
