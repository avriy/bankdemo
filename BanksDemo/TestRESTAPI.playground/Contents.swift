
import Foundation
import PlaygroundSupport

enum WebPath: String {
    case estonia = "https://www.swedbank.ee/finder.json"
    case latvia = "https://ib.swedbank.lv/finder.json"
    case lithuania = "https://ib.swedbank.lt/finder.json"
    
    var url: URL {
        return URL(string: rawValue)!
    }
    
}

extension WebPath {
    private static let swedEmbeeded = "Swedbank-Embedded"
    private static let iphoneApp = "iphone-app"
    
    func cookie() -> HTTPCookie {
        return HTTPCookie(properties: [ .name: WebPath.swedEmbeeded, .value: WebPath.iphoneApp, .path: "/", .originURL: url])!
    }
}

extension WebPath {
    func dataTask(successHandler: @escaping (WebPath, Data) -> Void) -> URLSessionDataTask {
        return URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let data = data {
                return successHandler(self, data)
            }
        }
    }
}

enum CodingErrors: Error {
    case noValue(String)
    case wrongType(String)
    case failedInitializeRawType
}

extension Dictionary where Key: CustomStringConvertible {
    
    func value<KeyType: RawRepresentable, ResultType>(for key: KeyType) throws -> ResultType
        where KeyType.RawValue == String {
        return try value(for: key.rawValue)
    }
    
    func value<T>(for key: String) throws -> T {
        guard let value = self[key as! Key] else {
            throw CodingErrors.noValue(key)
        }
        guard let castedValue = value as? T else {
            throw CodingErrors.wrongType(key)
        }
        
        return castedValue
    }
    
    func rawValue<KeyType: RawRepresentable, ResultType: RawRepresentable>(for key: KeyType) throws -> ResultType
        where KeyType.RawValue == String {
        return try rawValue(for: key.rawValue)
    }
    
    func rawValue<ResultType: RawRepresentable>(for key: String) throws -> ResultType {
        let rawValue: ResultType.RawValue = try value(for: key)
        guard let result = ResultType(rawValue: rawValue) else {
            throw CodingErrors.failedInitializeRawType
        }
        return result
    }
    
    func optionalValue<ResultType>(for key: String) -> ResultType? {
        return self[key as! Key] as? ResultType
    }
    
    func optionalValue<KeyType: RawRepresentable, ResultType>(for key: KeyType) -> ResultType?
        where KeyType.RawValue == String {
        return optionalValue(for: key.rawValue)
    }
    
}

struct BankLocation {

    enum LocationType: Int {
        case branch, atm, bna
    }
    
    let latitude: Double
    let longitude: Double
    let name: String
    let address: String
    let type: LocationType
    
    let region: String
    let availability: String?
    
    let info: String?
    let noCash: Bool?
    let hasCoinStation: Bool?
    
    enum Keys: String {
        case latitude = "lat", longitude = "lon", name = "n", adress = "a", type = "t",
        region = "r", availability = "av", info = "i", noCash = "ncash", hasCoinStation = "sc"
    }
    
    init(dictionary dic: [String : Any]) throws {
        latitude = try dic.value(for: Keys.latitude)
        longitude = try dic.value(for: Keys.longitude)
        name = try dic.value(for: Keys.name)
        address = try dic.value(for: Keys.adress)
        type = try dic.rawValue(for: Keys.type)
        region = try dic.value(for: Keys.region)
        availability = dic.optionalValue(for: Keys.availability)
        
        info = dic.optionalValue(for: Keys.info)
        noCash = dic.optionalValue(for: Keys.noCash)
        hasCoinStation = dic.optionalValue(for: Keys.hasCoinStation)
    }
}

let paths = [WebPath.estonia, .latvia, .lithuania]
let cookies = paths.map { $0.cookie() }

cookies.forEach {
    HTTPCookieStorage.shared.setCookie($0)
}

let dataTasks = paths.map {
    $0.dataTask(successHandler: { (path, data) in
        let objects = try! JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [[String : Any]]
//        print("objects are \(objects)")
        let locations: [BankLocation] = objects.flatMap { try? BankLocation(dictionary: $0) }
        
        print("number of locations is \(locations.count)")
        
        
    })
}

dataTasks.forEach {
    $0.resume()
}

PlaygroundPage.current.needsIndefiniteExecution = true

