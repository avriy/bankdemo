//
//  RegionsTVC.swift
//  BanksDemo
//
//  Created by Artemiy Sobolev on 30/10/2016.
//  Copyright © 2016 Artemiy Sobolev. All rights reserved.
//

import UIKit
import CoreData

class RegionCell: UITableViewCell {
    var region: Region? {
        didSet {
            textLabel?.text = region?.name
        }
    }
}

final
class RegionsTVC: UITableViewController {
    
    let updater = LocationsUpdater()
    var fetchResultsUpdater: FetchedResultsUpdater<Region>!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        refreshControl = UIRefreshControl()
        refreshControl?.addTarget(self, action: #selector(refreshControlWasPulled(control:)), for: .valueChanged)
        
        let fetchedRequest: NSFetchRequest<Region> = Region.fetchRequest()
        fetchedRequest.sortDescriptors = [NSSortDescriptor(key: "country.name", ascending: true), NSSortDescriptor(key: "name", ascending: true)]
        
        fetchResultsUpdater = FetchedResultsUpdater(context: CoreDataStack.shared.persistentContainer.viewContext, fetchRequest: fetchedRequest, sectionNameKeyPath: "country.name", cache: nil)
        
        fetchResultsUpdater.performFetch(reloadHandlers: tableView.reloadHandlers(with: .automatic))
        
        updater.update(completionHandler: loadCompletionHandler, errorHandler: loadErrorHandler)
    }
    
    var loadCompletionHandler: (Void) -> Void {
        return { [weak self] in
            self?.refreshControl?.endRefreshing()
        }
    }
    
    var loadErrorHandler: (NSError) -> Void {
        return { [weak self] error in
            let errorAlertController = UIAlertController.forError(error: error)
            self?.present(errorAlertController, animated: true, completion: nil)
        }
    }
 
    func refreshControlWasPulled(control: UIRefreshControl) {
        updater.update(completionHandler: loadCompletionHandler, errorHandler: loadErrorHandler)
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return fetchResultsUpdater.numberOfSections() ?? 0
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fetchResultsUpdater.numberOfObjects(forSection: section) ?? 0
    }
    
    static let regionCellID = "RegionCellID"
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return tableView.dequeueReusableCell(withIdentifier: RegionsTVC.regionCellID)!
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let cell = cell as? RegionCell else {
            fatalError("Cell has a wrong type")
        }
        cell.region = fetchResultsUpdater.item(indexPath: indexPath)
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return fetchResultsUpdater.sectionName(atIndex: section)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let rltvc = segue.destination as? RegionLocationsTVC, let cell = sender as? RegionCell {
            rltvc.region = cell.region
        }
    }
}
