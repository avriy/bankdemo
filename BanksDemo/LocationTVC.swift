//
//  LocationTVC.swift
//  BanksDemo
//
//  Created by Artemiy Sobolev on 30/10/2016.
//  Copyright © 2016 Artemiy Sobolev. All rights reserved.
//

import UIKit

class LocationTVC: UITableViewController {
    
    var location: Location!
    
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var regionLabel: UILabel!
    @IBOutlet weak var availabilityLabel: UILabel!
    @IBOutlet weak var infoLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = location.name
        
        typeLabel.text = location.locationType.description
        nameLabel.text = location.name
        addressLabel.text = location.address
        regionLabel.text = location.region?.name
        availabilityLabel.text = location.availability
        infoLabel.text = location.info
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath == IndexPath(item: 0, section: 1) && location.availability == nil {
            return 0
        }
        
        if indexPath == IndexPath(item: 1, section: 1) && location.info == nil {
            return 0
        }
        
        return super.tableView(tableView, heightForRowAt: indexPath)
    }
}
