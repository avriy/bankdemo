//
//  UIAlertController+Error.swift
//  BanksDemo
//
//  Created by Artemiy Sobolev on 30/10/2016.
//  Copyright © 2016 Artemiy Sobolev. All rights reserved.
//

import UIKit

extension UIAlertController {
    static func forError(error: NSError) -> UIAlertController {
        let result = UIAlertController(title: "Error", message: "\(error.localizedDescription)", preferredStyle: .alert)
        result.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        return result
    }
}
