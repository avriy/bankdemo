//
//  CodingHelpers.swift
//  
//
//  Created by Artemiy Sobolev on 27.10.16.
//
//

import Foundation

enum CodingErrors: Error {
    case noValue(String)
    case wrongType(String)
    case failedInitializeRawType
}

extension Dictionary where Key: CustomStringConvertible {
    
    func value<KeyType: RawRepresentable, ResultType>(for key: KeyType) throws -> ResultType
        where KeyType.RawValue == String {
            return try value(for: key.rawValue)
    }
    
    func value<T>(for key: String) throws -> T {
        guard let value = self[key as! Key] else {
            throw CodingErrors.noValue(key)
        }
        guard let castedValue = value as? T else {
            throw CodingErrors.wrongType(key)
        }
        
        return castedValue
    }
    
    func rawValue<KeyType: RawRepresentable, ResultType: RawRepresentable>(for key: KeyType) throws -> ResultType
        where KeyType.RawValue == String {
            return try rawValue(for: key.rawValue)
    }
    
    func rawValue<ResultType: RawRepresentable>(for key: String) throws -> ResultType {
        let rawValue: ResultType.RawValue = try value(for: key)
        guard let result = ResultType(rawValue: rawValue) else {
            throw CodingErrors.failedInitializeRawType
        }
        return result
    }
    
    func optionalValue<ResultType>(for key: String) -> ResultType? {
        return self[key as! Key] as? ResultType
    }
    
    func optionalValue<KeyType: RawRepresentable, ResultType>(for key: KeyType) -> ResultType?
        where KeyType.RawValue == String {
            return optionalValue(for: key.rawValue)
    }
    
}

extension Dictionary {
    func appending(by key: Key, value: Value) -> [Key : Value] {
        var copy = self
        copy[key] = value
        return copy
    }
}
