//
//  UITableView+ReloadHandlers.swift
//  BanksDemo
//
//  Created by Artemiy Sobolev on 29/10/2016.
//  Copyright © 2016 Artemiy Sobolev. All rights reserved.
//

import UIKit

public
extension UITableView {
    func reloadHandlers(with animation: UITableViewRowAnimation) -> ReloadHandlers {
        
        let insertItems: ([IndexPath]) -> Void = { [weak self] indexPaths in
            self?.insertRows(at: indexPaths, with: animation)
        }
        
        let deleteItems: ([IndexPath]) -> Void = { [weak self] indexPaths in
            self?.deleteRows(at: indexPaths, with: animation)
        }
        
        let updateItems: ([IndexPath]) -> Void = { [weak self] indexPaths in
            self?.reloadRows(at: indexPaths, with: animation)
        }
        
        let moveItems: (_ fromIndexPath: IndexPath, _ toIndexPath: IndexPath) -> Void = { [weak self] (from, to) in
            self?.moveRow(at: from, to: to)
        }
        
        let insertSection: (Int) -> Void = { [weak self] sectionIndex in
            self?.insertSections(IndexSet(integer: sectionIndex), with: animation)
        }
        
        let deleteSection: (Int) -> Void = { [weak self] sectionIndex in
            self?.deleteSections(IndexSet(integer: sectionIndex), with: animation)
        }
        
        let updateSection: (Int) -> Void = { [weak self] sectionIndex in
            self?.reloadSections(IndexSet(integer: sectionIndex), with: animation)
        }
        
        let update: (Void) -> Void = { [weak self] _ in
            self?.reloadData()
        }
        
        let willStartUpdates: (Void) -> Void = { [weak self] in
            self?.beginUpdates()
        }
        
        let didEndUpdates: (Void) -> Void = { [weak self] in
            self?.endUpdates()
        }
        
        return ReloadHandlers(insertItemsAtIndexPaths: insertItems,
                              deleteItemsAtIndexPaths: deleteItems,
                              updateItemsAtIndexPaths: updateItems,
                              moveItems: moveItems,
                              insertSection: insertSection,
                              deleteSection:deleteSection,
                              updateSection: updateSection,
                              update: update,
                              willStartUpdates: willStartUpdates,
                              didEndUpdates: didEndUpdates)
    }
}
