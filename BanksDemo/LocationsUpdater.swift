//
//  LocationsUpdater.swift
//  BanksDemo
//
//  Created by Artemiy Sobolev on 29/10/2016.
//  Copyright © 2016 Artemiy Sobolev. All rights reserved.
//

import Foundation
import CoreData

final
class LocationsUpdater {
    
    static func updateContry(country: BankCountry, context: NSManagedObjectContext) throws -> Country {
        
        let countryFetchRequest: NSFetchRequest<Country> = Country.fetchRequest()
        countryFetchRequest.predicate = NSPredicate(format: "name = %@", country.rawValue)
        countryFetchRequest.sortDescriptors = [NSSortDescriptor(key: "name", ascending: true)]
        let result = try context.fetch(countryFetchRequest)
        
        if let firstResult = result.first {
            return firstResult
        } else {
            let result = Country(context: context)
            result.name = country.rawValue
            return result
        }
    }
    
    static func updateRegions(for country: Country, regions: [String], context: NSManagedObjectContext) throws -> [Region] {
        let localRegionDictionary: [String : Region] = (country.regions as! Set<Region>).reduce([:]) { (result, element) in
            return result.appending(by: element.name!, value: element)
        }
        
        let remoteRegionNameSet = Set(regions)
        let localRegionSet = Set(localRegionDictionary.keys)
        
        let idsToAdd = remoteRegionNameSet.subtracting(localRegionSet)
        let idsToDelete = localRegionSet.subtracting(remoteRegionNameSet)
        let idsToKeep = remoteRegionNameSet.intersection(localRegionSet)
        
        let regionsToKeep = idsToKeep.flatMap { localRegionDictionary[$0] }
        
        var regionsCreated = [Region]()
        
        idsToAdd.forEach {
            let newRegion = Region(context: context)
            newRegion.name = $0
            newRegion.country = country
            regionsCreated.append(newRegion)
        }
        
        
        idsToDelete.forEach {
            let regionToDelete = localRegionDictionary[$0]!
            context.delete(regionToDelete)
        }
        
        return regionsToKeep + regionsCreated
    }
    
    static func updateLocations(for region: Region, locations: [BankLocation], context: NSManagedObjectContext) {
        
        let localLocations: [Int64 : Location] = (region.locations as! Set<Location>).reduce([:]) { (result, location) in
            var copy = result
            copy[location.remoteStamp] = location
            return copy
        }
        
        let remoteLocations: [Int64 : BankLocation] = locations.reduce([:]) { (result, location) in
            var copy = result
            copy[Int64(location.hashValue)] = location
            return copy
        }
        
        let localIDsSet = Set(localLocations.keys)
        let remoteIDsSet = Set(remoteLocations.keys)
        
        let idsToDelete = localIDsSet.subtracting(remoteIDsSet)
        let idsToAdd = remoteIDsSet.subtracting(localIDsSet)
        
        
        for id in idsToDelete {
            context.delete(localLocations[id]!)
        }
        
        for id in idsToAdd {
            let bankLocation = remoteLocations[id]!
            let newLocation = Location(context: context)
            newLocation.remoteStamp = id
            newLocation.region = region
            newLocation.latitude = bankLocation.latitude
            newLocation.longitude = bankLocation.longitude
            newLocation.address = bankLocation.address
            newLocation.locationType = bankLocation.type
            newLocation.name = bankLocation.name
            newLocation.availability = bankLocation.availability
            newLocation.info = bankLocation.info
            newLocation.noCash = bankLocation.noCash
            newLocation.hasCoinStation = bankLocation.hasCoinStation
        }
    }
    
    static func updateLocations(for regions: [Region], locationDictionary: [String : [BankLocation]], context: NSManagedObjectContext) {
        for region in regions {
            let locations = locationDictionary[region.name!]!
            updateLocations(for: region, locations: locations, context: context)
        }
    }
    
    
    static func executeInsertion(to context: NSManagedObjectContext, country: BankCountry, locations: [BankLocation]) throws {
        let country = try updateContry(country: country, context: context)
        
        let locationsDictionary: [String : [BankLocation]] = locations.reduce([:]) { (result, element) in
            let newValue = (result[element.region] ?? []) + [element]
            return result.appending(by: element.region, value: newValue)
        }
        let remoteRegionIDs = Array(locationsDictionary.keys)
        
        let regions = try updateRegions(for: country, regions: remoteRegionIDs, context: context)
        
        updateLocations(for: regions, locationDictionary: locationsDictionary, context: context)
        
    }
    
    private func insert(group: DispatchGroup, country: BankCountry, locations: [BankLocation], errorHandler eh: @escaping ErrorHandler) {
        group.enter()
        CoreDataStack.shared.performTaskWithBackgroundContext { context in
            do {
                try LocationsUpdater.executeInsertion(to: context, country: country, locations: locations)
                try context.save()
            } catch let error as NSError {
                eh(error)
            }
            group.leave()
        }
        
        group.notify(queue: .main) {
            CoreDataStack.shared.saveContext()
        }
    }
    
    
    init() {
        let configuration = URLSessionConfiguration.default
        configuration.requestCachePolicy = .reloadIgnoringLocalCacheData
        let session = URLSession(configuration: configuration)
        loader = LocationsLoader(session: session, countries: [.estonia, .latvia, .lithuania])
    }
    
    let loader: LocationsLoader
    func update(completionHandler ch: @escaping (Void) -> Void, errorHandler eh: @escaping ErrorHandler) {
        
        let syncGroup = DispatchGroup()
        
        let countryInsertionHandler: (BankCountry, [BankLocation]) -> Void = { [weak self] in
            self?.insert(group: syncGroup, country: $0, locations: $1, errorHandler: eh)
        }
        
        loader.load(perCountryHandler: countryInsertionHandler) { errors in
            if let firstError = errors.first {
                eh(firstError)
            }
            ch()
        }
    }
}

